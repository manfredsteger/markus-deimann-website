---
title: Philosophie
testimonials:
    - author: Markus Deimann
      content: Facebook will uns nur vordergründig mit anderen Menschen vernetzen, eigentlich geht es darum, zur zentralen Anlaufstelle im Netz zu werden. Ein Ausflug in das wilde WWW, das um Facebook herum tobt, ist nicht mehr notwendig.
    - author: Markus Deimann
      content: Der Kapitalismus gräbt sich immer tiefer in Bildung und Kultur ein und codiert dabei die Werte um.
---
