---
title: Vita
sections:
    - title: Bildung
      css_class: education
      items:
        - title: FernUniverstität in Hagen
          info: Habilitation: Open Education – Gegenstand, Theorie, Diskurs
          date: 2017
          description:  In meiner Habilitation habe ich mich mit der Öffnung von Bildung (Open Education) aus bildungswissneschaftlicher Perspektive. Zunächst ging es um die Klärung von Begriffen und Konzepten, danach wurden die verschiedenen Phasen der Öffnung aufgearbeitet. Aus Sicht von Bildungsthreorie ging es dann um eine Einordnung und Einschätzung. Abgerundet ist die Arbeit durch eine Architektur offener Hochschule.
        - title: Universität Erfurt/Technische Universität Ilmenau
          info: Promotion: Entwicklung und Erprobung eines volitionalen Designmodells
          date: 2007
          description: Das waren meine ersten Stationen nach dem Studium und ich arbeitete in einem Forschungs- und Entwicklungsprojekt zum Thema "Neue Medien in der Bildung". Ich arbeitete mich in das Thema "E-Learning" ein und sollte später merken, dass viele Konzepte, die heute als neu ausgegeben werden, schon damals diskutiert wurden. Geschichte wiederholt sich doch.
        - title: Universtität Mannheim
          info: Studium: Erziehungswissenschaft und Politikwissenschaft
          date: 2001
          description: An der Universität Mannheim studierte ich Erziehungswissenschaft und Politikwissenschaft mit dem Abschluss Magister Artium (M.A.). Das war eine völlig neuer Erfahrung für mich, nach 13 Jahre Beschulung mich selbstständig in Themen einzuarbeiten und meinen Interessen nachgehen zu können. Neben der inhaltlichen Beschäftigung war die Universität für mich vorallem der Ort für intellektuellen Austausch. Das sollte noch eine lange Zeit so bleiben...
    - title: Arbeitgeber
      css_class: work
      items:
        - title: Technische Hochschule Lübeck, Institut für Lerndienstleistungen (ILD)
          info: Head of Research und MOOC Maker
          date: 2016 - bis jetzt
          description:  Am ILD werden viele Projekte (EU, Bund, Land) bearbeitet, ich koordiniere die Forschungsaktivitäten. 
        - title: FernUniversität in Hagen
          info: Akademischer Rat (Assistant Professor)
          date: 2013 - 2016
          description:  This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat
        - title: FernUniversität in Hagen
          info: Akademischer Rat/wissenschaftlicher Assistent
          date: 2006 - 2016
          description:  Insgesamt 10 Jahre war ich in verschiedenen Rollen an der FernUniversität beschäftigt. Konstant geblieben ist meine Tätigkeit in der Lehre, wo ich in den Studiengängen "Bildung und Medien" und "Bildungswissenschaft" BA- und MA-Arbeiten betreute. In Bereich der Forschung habe ich zu Beginn meine Dissertation abgeschlossen und mich dann dem Thema Offene Bildung/Open Education gewidmet. Dazu habe ich viele Artikel geschrieben und Vorträge gehalten und so eine eigene bildungswissenschaftliche Perspektive entwickelt. Abgeschlossen wurde das Projekt mit meiner Habilitation.
        - title: Open University UK
          info: Research Fellow
          date: 2011
          description:  Im Rahmen eines Stipendiums arbeitete ich im Projekt Open Learn der Open University UK und beschäftigte mich mit der Frage, wie sich die Öffnung von pädagogischen Materialien und Prozessen auf die Bildung auswirkt. Dazu habe ich u.a. den Artikel "<a href="http://www.irrodl.org/index.php/irrodl/article/view/1370/2608" title="Rethinking OER and their use: Open education as Bildung" rel="bookmark">Rethinking OER and their use: Open education as Bildung</a>" verfasst.
        - title: Florida State University, USA
          info: Visiting Scholar
          date: 2004
          description:  Als Visiting Scholar habe ich an meiner Dissertation gearbeitet und mit der Arbeitsgruppe von John Keller eine Reihe von Studien durchgeführt. Auch habe ich an verschiedenen Lehrveranstaltungen zu den Themen Instructional Design und Pädagogische Psychologie teilgenommen und mich in das Studentenleben auf dem Campus gestürzt.
    - title: Kompetenzen
      css_class: skill
      items:
        - title:
          info:
          date:
          description: Ich schreibe gerne, mische mich mit unbequemen Positionen provokant in Debatten ein. Ich denke analytisch, erfasse schnell Zusammenhänge und kann komplexe Dinge verständlich erklären.
          skills:
            - name: Bildung
              level: 65
            - name: Arbeit
              level: 50
            - name: Didaktik
              level: 50
            - name: Medien
              level: 95
            - name: International
              level: 80
            - name: Forschung
              level: 60
---
