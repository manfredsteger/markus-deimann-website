---
title: person
title2: Kontaktinformationen
address:
    -
        line: 'Markus Deimann'
    -
        line: 'Fleischhauerstraße 40'
    -
        line: '23552 Lübeck'
email:
    -
        address: markusdeimann@markusdeimann.de
buttons:
    -
        url: user/pages/01.home/_about/vita-pd-dr-markus-deimann.pdf
        icon: download
        text: 'Vita herunterladen'
---

## Über mich

Ich bin habilitierter Bildungswissenschaftler und beschäftige mich seit über 15 Jahren mit den Auswirkungen von Technik und Medien auf das Lehren und Lernen. Früher nannte man es E-Learning, heute heißt es Digitalisierung der Bildung. Regelmäßig tauchen neue Begriffe auf, die Ideen dahinter sind nicht immer neu. Innovationen sind allgegenwärtig, gründliches Nachdenken kommt erst danach, wenn überhaupt. 